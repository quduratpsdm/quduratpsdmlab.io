const tooltip = document.getElementById('tooltip');

tooltip.addEventListener('click', () => {
    document.fullscreenElement 
        ? document.exitFullscreen?.() 
        : document.documentElement.requestFullscreen();
});

document.addEventListener('fullscreenchange', () => {
    tooltip.src = document.fullscreenElement 
        ? 'images/closeFullscreen.svg' 
        : 'images/openFullscreen.svg';
});

